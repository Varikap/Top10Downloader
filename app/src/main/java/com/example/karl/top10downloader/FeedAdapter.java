package com.example.karl.top10downloader;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class FeedAdapter extends ArrayAdapter {
    private static final String TAG = "FeedAdapter";
    private final int layoutResource;
    private final LayoutInflater layoutInflater;
    private List<FeedEntry> apps;

    public FeedAdapter(@NonNull Context context, int resource, List<FeedEntry> apps) {
        super(context, resource);
        this.layoutResource = resource;
        this.layoutInflater = LayoutInflater.from(context);
        this.apps = apps;
    }

    /*
     * This method must be overwritten!!
     * */
    @Override
    public int getCount() {
        return apps.size();
    }

    @NonNull
    @Override
    // if listview have item that can reuse, it passes reference to it in convertView
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        /*
        * Old method.
        * If you dont use convertView app is not effisient
        *

        View view = layoutInflater.inflate(layoutResource,parent,false); //parent will be list_record.xml
        TextView tvName = (TextView) view.findViewById(R.id.tvName); // you need this view.findView cause it needs id from that view
        TextView tvArtist = (TextView) view.findViewById(R.id.tvArtist); // find view that is part of view
        TextView tvSummary = (TextView) view.findViewById(R.id.tvSummary);

        FeedEntry currentApp = apps.get(position);

        tvName.setText(currentApp.getName());
        tvArtist.setText(currentApp.getArtist());
        tvSummary.setText(currentApp.getSummary());

        return view; */
        /*
         * After running test, app uses around 50 MBs by first method, after code below, app runs for max 30 MBs
         * Test was runned on 200 aps without TextView tvSummary
         * */

        //if you get annoyed by viewHolder, you can do it without just go without else block and viewholder lines in if
        //but this improve smoothness of scrolling
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(layoutResource,parent,false);

            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        FeedEntry currentApp = apps.get(position);

        viewHolder.tvName.setText(currentApp.getName());
        viewHolder.tvArtist.setText(currentApp.getArtist());
        viewHolder.tvSummary.setText(currentApp.getSummary());

        return convertView;
    }

    private class ViewHolder {
        final TextView tvName;
        final TextView tvArtist;
        final TextView tvSummary;

        ViewHolder (View v) {
            this.tvName = v.findViewById(R.id.tvName);
            this.tvArtist = v.findViewById(R.id.tvArtist);
            this.tvSummary = v.findViewById(R.id.tvSummary);
        }
    }

}
